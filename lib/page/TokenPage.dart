import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:intl/intl.dart';
import 'package:onetimepassword/utils/pref_manager.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class TokenPage extends StatefulWidget {

  @override
  TokenPageState createState() => TokenPageState();
}

class TokenPageState extends State<TokenPage> {
  double shortTestside;
  int bottomSelectedIndex = 0;

  int num1 = 0, num2 = 0, num3 = 0, num4 = 0, num5 = 0, num6 = 0;
  int endTime = 0;
  int fullTime = 180; // sec
  CountdownTimerController controller;
  Timer _timer;
  double percent = 0.0;
  String strPercent = "0%";

  @override
  void initState() {
    Prefs.load().then((value) => {
      checkValuePrefs(),
    });
    super.initState();
  }

  checkValuePrefs(){
    if(Prefs.getString(Prefs.PREF_AUTHEN) != null){
      String token = Prefs.getString(Prefs.PREF_AUTHEN);
      if(token.isEmpty){
        _clearNumber();
      } else {
        setState(() {
          num1 = int.parse(token[0]);
          num2 = int.parse(token[1]);
          num3 = int.parse(token[2]);
          num4 = int.parse(token[3]);
          num5 = int.parse(token[4]);
          num6 = int.parse(token[5]);
        });
        String time = Prefs.getString(Prefs.PREF_TIMEOUT_TOKEN);
        print('time : ${time}');
        if(!time.isEmpty){
          var now = DateTime.now();
          var dateStart = DateTime.parse(Prefs.getString(Prefs.PREF_TIME_TOKEN));
          var dateTo = DateTime.parse(Prefs.getString(Prefs.PREF_TIMEOUT_TOKEN));
          if(dateTo.isAfter(now)){
            fullTime = dateTo.difference(dateStart).inSeconds;
            int balanc = fullTime - dateTo.difference(now).inSeconds;
            endTime = DateTime.now().millisecondsSinceEpoch + 1000 * (fullTime - balanc);
            controller = CountdownTimerController(endTime: endTime, onEnd: onEnd);
          }
        } else {
          _clearNumber();
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    shortTestside = MediaQuery.of(context).size.shortestSide;
    return Scaffold(
      appBar: AppBar(
        elevation: 3.0,
        title: Text('Token', style: TextStyle(color: Colors.white,
            fontSize: shortTestside / 15, fontWeight: FontWeight.bold)
        ),
        backgroundColor: Colors.black,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                    Icons.more_vert
                ),
              )
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.blueGrey,
                  Colors.black87,
                ],
              ),
            ),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    layoutNumberToken(),
                    CountdownTimer(
                      controller: controller,
                      endTime: endTime,
                      widgetBuilder: (_, CurrentRemainingTime time) {
                        if (time == null) {
                          setProgressValue(0, "0%");
                          return layoutProgress("Token complete");
                        } else {
                          if(controller.isRunning){
                            // _timer =  Timer(Duration(seconds: 1), () => { _randomNumber() });
                          } else {
                            setProgressValue(0, "0%");
                            return layoutProgress("Token complet");
                          }
                        }
                        sumTimeProgress(time);
                        if(time.min != null){
                          return layoutProgress('Time : ${time.min} min ${time.sec} sec');
                        } else {
                          return layoutProgress('Time : ${time.sec} sec');
                        }
                      },
                    ),
                  ],
                ),
              ),
              Container(

              ),
            ],
          )
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blueGrey,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white,
        currentIndex: bottomSelectedIndex,
        items: buildBottomNavBarItems(),
        onTap: (index) {
          bottomTapped(index);
        },
      ),
    );
  }

  sumTimeProgress(CurrentRemainingTime time){
    int sum = 0;
    if(time.min != null){
      sum += time.min * 60;
    }
    sum += time.sec;
    double _percent = ((fullTime - sum) / fullTime) * 100;
    setProgressValue(_percent / 100, '${_percent.toStringAsFixed(0)}%');
  }

  setProgressValue(double _progress, String _percent){
    percent = _progress;
    strPercent = _percent;
  }

  Widget layoutNumberToken(){
    return Container(
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _BoxTextNumber(num1),
          _BoxTextNumber(num2),
          _BoxTextNumber(num3),
          _BoxTextNumber(num4),
          _BoxTextNumber(num5),
          _BoxTextNumber(num6),
        ],
      ),
    );
  }

  Widget _BoxTextNumber(int number){
    return Expanded(
        child: Container(
          width: shortTestside / 7,
          height: shortTestside / 6,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Colors.black87,
                Colors.blueGrey,
              ],
            ),
          ),
          margin: EdgeInsets.all(10),
          alignment: Alignment.center,
          child: Text(number.toString(), style:TextStyle(color: Colors.white, fontSize: 25,
              fontWeight: FontWeight.bold)
          ),
        )
    );
  }

  Widget layoutProgress(String status){
    return Container(
      margin: EdgeInsets.only(left: shortTestside / 25),
      child: Row(
        children: <Widget>[
          new CircularPercentIndicator(
            radius: 60.0,
            lineWidth: 5.0,
            percent: percent,
            center: new Text(strPercent, style: TextStyle(color: Colors.white)),
            progressColor: Colors.lightBlue,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: shortTestside / 25),
              child: Text(status,
                style: TextStyle(fontSize: shortTestside / 15, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<BottomNavigationBarItem> buildBottomNavBarItems() {
    return [
      BottomNavigationBarItem(
          icon: new Icon(Icons.loop_sharp),
          label: "Refresh token"
      ),
      BottomNavigationBarItem(
          icon: new Icon(Icons.stop_circle),
          label: "Stop"
      ),
    ];
  }

  void bottomTapped(int index) {
    if(index == 0){
      var now = DateTime.now();
      var dateTo = now.add(Duration(seconds: fullTime));
      String nowSave = DateFormat('yyyy-MM-dd kk:mm:ss').format(now);
      String dateSave = DateFormat('yyyy-MM-dd kk:mm:ss').format(dateTo);
      Prefs.setString(Prefs.PREF_TIME_TOKEN, nowSave);
      Prefs.setString(Prefs.PREF_TIMEOUT_TOKEN, dateSave);
      endTime = DateTime.now().millisecondsSinceEpoch + 1000 * fullTime;
      controller = CountdownTimerController(endTime: endTime, onEnd: onEnd);
      _timer =  Timer(Duration(seconds: 1), () => { _randomNumber() });
    } else if(index == 1) {
      _clearNumber();
      if(controller != null){
        controller.disposeTimer();
      }
      _timer.cancel();
    }
    setState(() {
      bottomSelectedIndex = index;
      // pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  _randomNumber(){
    var rng = new Random();
    setState(() {
      num1 = rng.nextInt(9);
      num2 = rng.nextInt(9);
      num3 = rng.nextInt(9);
      num4 = rng.nextInt(9);
      num5 = rng.nextInt(9);
      num6 = rng.nextInt(9);
    });
    Prefs.setString(Prefs.PREF_AUTHEN, '${num1}${num2}${num3}${num4}${num5}${num6}');
  }

  _clearNumber(){
    var rng = new Random();
    setState(() {
      num1 = 0;
      num2 = 0;
      num3 = 0;
      num4 = 0;
      num5 = 0;
      num6 = 0;
    });
    Prefs.setString(Prefs.PREF_AUTHEN, '${num1}${num2}${num3}${num4}${num5}${num6}');
    Prefs.setString(Prefs.PREF_TIMEOUT_TOKEN, "");
    Prefs.setString(Prefs.PREF_TIME_TOKEN, "");
  }

  void onEnd() {
    print('onEnd');
  }

  @override
  void dispose() {
    if(controller != null){
      controller.dispose();
    }
    if(_timer != null){
      _timer.cancel();
    }
    super.dispose();
  }
}